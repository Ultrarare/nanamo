pub mod core;
pub mod jobs;
pub mod logger;

extern crate argparse;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use argparse::{ArgumentParser, Store, StoreTrue};

static mut DEBUG: bool = false;

fn main() {
    let mut profile = "".to_string();
    {  // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Nanamo - FFXIV Sim");
        ap.refer(&mut profile)
            .add_option(&["-p", "--profile"], Store,
            "Profile to run a sim against");
        unsafe {
            ap.refer(&mut DEBUG)
                .add_option(&["-v", "--verbose"], StoreTrue,
                            "Prints verbose messages");
        }
        ap.parse_args_or_exit();
    }
    if profile != "" {
        core::sim::run(profile)
        // run the sim
    } else {
        println!("A profile must be loaded.")
    }
}
