use jobs::JobT;

use std::collections::HashMap;
use std::rc::Rc;

use core::ability::AbilityT;

pub struct Paladin {}

impl Paladin {
    pub fn load_abilities() -> HashMap<String, Rc<AbilityT>> {
        let map: HashMap<String, Rc<AbilityT>> = HashMap::new();
        map
    }
}

// Implements the required job related tasks for this class.
impl JobT for Paladin {
    fn execute(&self) {
        println!("Paladin doing something")
    }
}
