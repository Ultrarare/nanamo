use jobs::JobT;

use std::collections::HashMap;
use std::rc::Rc;

use core::ability::AbilityT;
use core::ability::Ability;

pub struct Bard {}

impl Bard {
    pub fn load_abilities() -> HashMap<String, Ability> {
        let mut map: HashMap<String, Ability> = HashMap::new();
        map.insert("heavy_shot".to_string(), Ability::new(Rc::new(HeavyShot {})));
        map.insert("auto_attack".to_string(), Ability::new(Rc::new(AutoAttack {})));
        map
    }
}

// Implements the required job related tasks for this class.
impl JobT for Bard {
    fn execute(&self) {
        println!("Bard doing something")
    }
}

pub struct AutoAttack {}
impl AbilityT for AutoAttack {
    fn recast(&self) -> u64 { 2500 }
    fn indef(&self) -> bool { true }
    fn trigger_gcd(&self) -> bool { false }
    fn execute(&self) { println!("Bard - EXECUTING AUTO ATTACK"); }
}

pub struct HeavyShot {}
impl AbilityT for HeavyShot {
    fn recast(&self) -> u64 { 2500 }
    fn indef(&self) -> bool { false }
    fn trigger_gcd(&self) -> bool { true }
    fn execute(&self) { println!("Bard - EXECUTING HEAVY SHOT");}
}
