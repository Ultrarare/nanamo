pub mod bard;
pub mod paladin;

use std::collections::HashMap;
use core::ability::Ability;
use core::profile::Profile;
use jobs::bard::Bard;
// use jobs::paladin::Paladin;

pub struct Job {
    pub data: Profile,
    pub abilities: HashMap<String, Ability>,
    pub next_gcd: u64,
}

impl Job {
    pub fn new(profile: Profile) -> Job {
        let abilities = match profile.job() {
            "bard" => Bard::load_abilities(),
            // "paladin" => Paladin::load_abilities(),
            _ => Bard::load_abilities()
        };
        Job {
            data: profile,
            abilities: abilities,
            next_gcd: 0,
        }
    }
    pub fn abilities(&mut self) -> &mut HashMap<String, Ability> {
        &mut self.abilities
    }
    pub fn get_ability(&self, name: &str) -> &Ability{
        return &self.abilities.get(name).unwrap();
    }
    pub fn calculate_gcd(&self) -> u64 {
        2500
    }
    pub fn set_gcd(&mut self, time: u64) {
        let new_time = self.calculate_gcd() + time;
        println!("@@@ ASSIGNING NEW GCD :: {}", new_time);
        self.next_gcd = new_time;
    }
    pub fn execute_ability(&mut self, time: u64, name: String) {
        let ability = self.get_ability(&name);
        println!("{} - {:?}", time, ability);
        println!("{} - Executing {}", time, name);
        ability.execute;
    }
}

pub trait JobT {
    fn execute(&self);
}
