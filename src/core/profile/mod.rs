use core::stats::Stats;

#[derive(Serialize, Deserialize)]
pub struct ActionList {
    pub precombat: Vec<String>,
    pub actions: Vec<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Profile {
    pub job: String,
    pub name: String,
    pub spec: String,
    pub level: i64,
    pub role: String,
    pub stats: Stats,
    pub action_list: ActionList,
}

impl Profile {
    pub fn job(&self) -> &str {
        &self.job
    }
    pub fn name(&self) -> &str {
        &self.name
    }
}
