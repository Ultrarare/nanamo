use logger::{debug};
use std::fs::File;
use std::io::prelude::*;
use serde_json;
use serde_json::Error;
use core::profile::Profile;

pub fn create_profile(file: &str) -> Result<Profile, Error> {
    let json = serde_json::from_str(&file);
    return json
}

pub fn load_profile(profile: &str) -> String {
    debug("Loading Profile");
    let mut f = File::open(profile).expect("profile not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the profile");
    debug(&contents);
    contents
}
