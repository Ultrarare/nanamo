use std::collections::BinaryHeap;
use std::cmp::Ordering;

pub struct Timer {
    pub time: u64,
    pub max_time: u64,
    pub events: BinaryHeap<Event>,
}

impl Timer {
    pub fn schedule_event(&mut self, time: u64, action: String, trigger_gcd: bool) {
        println!("{} - Scheduling {} to run at {}", &self.time, action, time);
        let event = Event { time: time, action: action, trigger_gcd: trigger_gcd };
        &self.events.push(event);
    }
}

#[derive(Debug)]

pub struct Event {
    pub time: u64,
    pub action: String,
    pub trigger_gcd: bool,
}

impl Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        other.time.cmp(&self.time).reverse()
    }

}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Event) -> Option<Ordering> {
        Some(self.cmp(other).reverse())
    }
}

impl PartialEq for Event {
    fn eq(&self, other: &Event) -> bool {
        self.time == other.time
    }
}
impl Eq for Event {}
