use std::rc::Rc;

pub trait AbilityT {
    fn recast(&self) -> u64;
    fn trigger_gcd(&self) -> bool;
    fn indef(&self) -> bool;
    fn execute(&self);
}

#[derive(Debug)]
pub struct Ability {
    pub recast: u64, // recast time as int 2.5s = 25000
    pub trigger_gcd: bool, // does this trigger this gcd?
    pub indef: bool, // indefinite traits like auto attacks and class systems
    pub execute: (),
}
impl Ability {
    pub fn new(ability: Rc<AbilityT>) -> Ability {
        Ability {
            recast: ability.recast(),
            trigger_gcd: ability.trigger_gcd(),
            indef: ability.indef(),
            execute: ability.execute(),
        }
    }
    pub fn recast(&self) -> u64 { self.recast }
    pub fn trigger_gcd(&self) -> bool { self.trigger_gcd }
    pub fn indef(&self) -> bool { self.indef }
    pub fn execute(&self) { self.execute }
}
