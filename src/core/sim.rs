use std::collections::HashMap;
use std::collections::BinaryHeap;

use jobs::Job;
use core::actor;
use core::timer::Timer;

pub fn run(profile: String) {
    let file = actor::load_profile(&profile);
    let prof = actor::create_profile(&file).unwrap();
    let mut actor = Job::new(prof);
    let mut timer = Timer { time: 0, max_time: 300000, events: BinaryHeap::new() };
    // run precombat
    //
    //
    //
    // schedule auto attack at 0 if is in the profile list - this is an indef
    // so it runs forever. We should probably look at nicer way of doing this
    timer.schedule_event(0, "auto_attack".to_string(), false);
    while timer.time < timer.max_time {
        println!("Start Scheduling");
        //// System Events

        // GCD reset

        // Actor Events
        let actions = actor.data.action_list.actions.clone();

        for a in actions {
            // Skipping auto attack here because its an indefinite event
            if a == "auto_attack" { continue; }
            let time = timer.time.clone();
            println!(":::: ACTOR GCD:: {}", actor.next_gcd < timer.time);
            if actor.next_gcd < timer.time && actor.get_ability(&a).trigger_gcd() { continue; }
            timer.schedule_event(time, a.to_string(), actor.get_ability(&a).trigger_gcd);
            // trigger a gcd if it triggers one
            if actor.get_ability(&a).trigger_gcd() {
                actor.set_gcd(timer.time)
            }
        }
        println!("-------HEAP--------");
        // debug
        let time = timer.time.clone();
        for event in &timer.events {
            println!("{:?}", event);
        }
        println!("-------/HEAP--------");
        // enddebug
        // Ability Handler
        println!("Start Handling");
        while timer.events.len() > 0 {
            // peek at next time and break the loop if we're done with this tick
            let peek = timer.events.peek().unwrap().time.clone();
            let peek_action = timer.events.peek().unwrap().action.clone();
            // quit if we're done simming
            if peek > timer.time { break }
            if timer.time >= timer.max_time { break };
            let action = timer.events.pop().unwrap().action;
            println!("POP  ::: Popping {}", action);
            actor.execute_ability(timer.time, action.to_string());
            if actor.get_ability(&action).trigger_gcd() { actor.set_gcd(timer.time); }

            println!("PEEK ::: Peeking {} - {}", peek_action, peek);
            println!("LENGTH LEFT: {}",timer.events.len());
        }
        // reschedule Auto Attack
        let reschedule_time = time + actor.get_ability("auto_attack").recast;
        println!("{} - Rescheduling {} to run at {}", time, "auto_attack", reschedule_time);
        timer.schedule_event(reschedule_time, "auto_attack".to_string(), false);
        // find next time and go
        timer.time = timer.events.peek().unwrap().time;
        println!("TIMER TIME IS NOW {}", timer.time);
    }
    // run handle
    // run next time on timer
}
