#[allow(dead_code)]

#[derive(Serialize, Deserialize)]
pub struct Stats {
    pub strength: i64,
    pub dexterity: i64,
    pub vitality: i64,
    pub intelligence: i64,
    pub mind: i64,
    pub piety: i64,
    pub accuracy: i64,
    pub crit: i64,
    pub determination: i64,
    pub attack_power: i64,
    pub skill_speed: i64,
    pub magic_potency: i64,
    pub healing_potency: i64,
    pub spell_speed: i64,
    pub defense: i64,
    pub parry: i64,
    pub magic_defense: i64,
    pub slashing: i64,
    pub piercing: i64,
    pub blunt: i64,
    pub morale: i64,
}
