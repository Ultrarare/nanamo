pub fn log(string: &str) {
    println!("{}", string.to_string());
}

pub fn debug(string: &str) {
    unsafe {
        if ::DEBUG {
            println!("DEBUG: {}", string.to_string());
        }
    }
}
